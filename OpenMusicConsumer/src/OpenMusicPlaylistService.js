const {Pool} = require('pg');
const { rows } = require('pg/lib/defaults');


class OpenMusicPlaylistService {
    constructor  () {
        this._pool = new Pool();
    }

    async getPlaylist (playlistId) {
         const query = {
            text: `SELECT songs.id, songs.title, songs.performer
            FROM songs
            JOIN playlistsongs
            ON songs.id = playlistsongs.song_id WHERE playlistsongs.playlist_id = $1`,
            values: [playlistId],
            };
         const queryGetPlaylist = {
            text : `SELECT playlists.id, playlists.name FROM playlists 
            JOIN users ON users.id = playlists.owner
            WHERE playlists.id = $1;`,
            values : [playlistId],
            };
        const playlist = await this._pool.query(queryGetPlaylist);
        const allSongByIdPlaylist = await this._pool.query(query);
        console.log(allSongByIdPlaylist.rows);
        let result = {'playlist' : playlist.rows[0]};
        result.playlist['songs'] = allSongByIdPlaylist.rows;
        console.log(result);
        //console.log(allSongByIdPlaylist);
        return result;
    }

}

module.exports = OpenMusicPlaylistService;