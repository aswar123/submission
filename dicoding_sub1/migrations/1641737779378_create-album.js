exports.shorthands = undefined;
 
exports.up = (pgm) => {
  pgm.createTable('album', {
    id: {
      type: 'VARCHAR(50)',
      primaryKey: true,
    },
    name: {
      type: 'TEXT',
      notNull: true,
    },
    year: {
      type: 'INTEGER',
      notNull: true,
    },
    cover : {
      type : 'VARCHAR(100)',
      notNull : false,
      },
    },
  );
};
exports.down = (pgm) => {
  pgm.dropTable('album');
};