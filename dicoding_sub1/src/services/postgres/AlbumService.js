const {Pool} = require("pg");

const {nanoid} = require('nanoid');

const {mapDBToModel} = require('../../utils');

const InvariantError = require('../exceptions/InvariantError');
const NotFoundError = require('../exceptions/NotFoundError');

class NoteService {
    constructor (cacheService, storageService) {
        this._pool = new Pool();
        this._cacheService = cacheService;
        this._storageService = storageService;
    }

    async AddAlbum (data) {
        const id = `Album-${nanoid(16)}`;

        const query = {
            text : 'INSERT INTO album VALUES ($1, $2, $3) RETURNING id',
            values : [id, data.name , data.year,],
        };

        let result = await this._pool.query(query);

        if (!result.rows[0].id) {
        throw new InvariantError('Catatan gagal ditambahkan');
    };
    return result.rows[0].id;

    }

    async insertToCover(albumId, fileName) {
        const query = {
            text : 'UPDATE album SET cover = $1 WHERE id = $2 RETURNING id',
            values : [fileName, albumId],
        };
        const result = await this._pool.query(query);
        if (!result.rows.length){
            throw new NotFoundError('Gagal menambahkan cover catatan. Id tidak ditemukan');
        }
    }

    async getAlbumById(id) {
        const queryAlbums = {
            text: 'SELECT id, name, year, cover AS coverUrl FROM album WHERE id = $1',
            values : [id],
        };
        const querySongs = {
            text : 'SELECT * FROM songs WHERE "albumId" = $1',
            values : [id],
        };
        const Song = await this._pool.query(querySongs);
        const Album = await this._pool.query(queryAlbums);
        const url = Album.rows[0].coverurl;
        let result = Album.rows[0];
        delete result.coverurl;
         if (!Album.rows.length){
            throw new NotFoundError('Catatan tidak di temukan');
         }
         console.log(result);
        if(url !== null) {
            const fileName = `http://${process.env.HOST}:${process.env.PORT}/src/api/uploads/file/images/${url}`;
            result['coverUrl'] = fileName;
        }else {
            result['coverUrl'] = null;
        }
        result['songs'] = Song.rows;
        return result;
        }

    async editAlbumByid(id, { name, year}) {
        const query = {
            text : 'UPDATE album SET name = $1, year = $2 WHERE id = $3 RETURNING id, name, year',
            values : [name, year, id],
        };

        const result = await this._pool.query(query);

        if (!result.rows.length){
            throw new NotFoundError('Gagal memperbarui catatan. Id tidak ditemukan');
        }
        return result.rows[0];
    }

    async deleteAlbumById (id) {
        const query = {
        text : 'DELETE FROM album WHERE id = $1 RETURNING id',
        values : [id],
        }

        const result = await this._pool.query(query); 

        if (!result.rows.length){
            throw new NotFoundError('Catatan gagal dihapus. Id tidak ditemukan');
        }
    }

    async likeAlbums(albumsId, idUsers) {
        const id = `LIKE-${nanoid(16)}`;
        const query = {
            text : 'INSERT INTO user_album_likes VALUES($1, $2, $3) RETURNING id',
            values : [id, idUsers, albumsId],
        };
        const result = await this._pool.query(query);
        if(!result.rows[0].id){
            throw new InvariantError('gagal like album');
        }

    }

    async cekLikeAlbums(albumsId, idUsers) {
        const query = {
            text : 'SELECT id FROM user_album_likes WHERE user_id =$1 AND album_id=$2',
            values : [idUsers, albumsId],
        };
        const result = await this._pool.query(query);
        if(!result.rows.length) {
            return false;
        }else {
            return true;
        }
    }


    async unlikeAlbums(albumsId, idUsers) {
        const query = {
            text : 'DELETE FROM user_album_likes WHERE user_id =$1 AND album_id=$2 RETURNING id',
            values : [idUsers, albumsId],
        };
        const result = await this._pool.query(query); 
        if(!result.rows[0].id){
            throw new InvariantError('gagal like album');
        }
    }

    async countLikeAlbums(albumsId) {
        try {
            const result = await this._cacheService.get(`likes${albumsId}`);
            console.log(`ini adalah cache ${result}`);
            return {
                data : JSON.parse(result),
                source : 'cache',
            };
        }catch (error) {
            const query = {
            text : 'SELECT COUNT(album_id) AS likes FROM user_album_likes WHERE album_id = $1',
            values : [albumsId],
        };
        const result = await this._pool.query(query);
        if(!result.rows[0].likes){
            throw new InvariantError('gagal like album');
        }
        await this._cacheService.set(`likes${albumsId}`, JSON.stringify(result.rows[0].likes));
        return {
            data : result.rows[0].likes,
            source : 'Database',
        };
        }
    }

    async verifyAlbumIdInDatabase(albumId) {
        const query = {
            text : 'SELECT * FROM album WHERE id = $1',
            values : [albumId],
        };
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new NotFoundError('Album tidak di temukan');
        }
} 

}

module.exports = NoteService;