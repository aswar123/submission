const {Pool} = require("pg");

const {nanoid} = require('nanoid');

const {mapDBToModel} = require('../../utils');

const InvariantError = require('../exceptions/InvariantError');
const NotFoundError = require('../exceptions/NotFoundError');
 
class SongService {
    constructor (){
        this._pool = new Pool();
    }
    async AddSong ({title, year, genre, performer, duration, albumid}) {
        const id = nanoid(16);
        const query = {
            text : 'INSERT INTO songs VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id',
            values : [id, title, year, genre, performer, duration, albumid],
        };

        const result = await this._pool.query(query);
        if (!result.rows[0].id) {
            throw new NotFoundError("Lagu gagal di tambahakan");
        }
        return result.rows[0].id;
    }


    async getSongs (title, performer) {

        if (title === undefined && performer === undefined){
             console.log("edrtfyuiop");
        const result = await this._pool.query('SELECT id, title, performer FROM songs');
        return result.rows;
        }
        else if (!title){
        const query = {
                text : 'SELECT * FROM songs WHERE performer = $1',
                values : [performer],
            };
            const result = await this._pool.query(query);
            return result.rows;
        }
        else if (!performer) {
            const query = {
                text : 'SELECT * FROM songs WHERE title = $1',
                values : [title],
            };
            const result = await this._pool.query(query);
            return result.rows;
        }
        else if (title !== undefined && performer !== undefined){
            
            const query = {
                text : 'SELECT * FROM songs WHERE title = $1 and performer = $2',
                values : [title, performer],
            };
            const result = await this._pool.query(query);
            return result.rows;
        }
    }

    async getSongsById (id) {
        const query = {
            text : 'SELECT * FROM songs WHERE id = $1',
            values : [id],
        };
        const result = await this._pool.query(query);

        if (!result.rows.length) {
            throw new NotFoundError('lagu tidak di temukan');
        }
        return result.rows[0];

    }

    async upadateSongsById (id, {title, year, genre, performer, duration, albumId}) {
        console.log(id);
        const query = {
            text : 'UPDATE songs SET title = $1, year = $2, genre = $3, performer = $4, duration = $5, "albumId"= $6 WHERE id = $7 RETURNING id',
            values : [title, year, genre, performer, duration, albumId, id],
        };
        const result = await this._pool.query(query);
        if (!result.rows.length) {
            throw new NotFoundError('lagu tidak di temukan');
        }
        console.log(result.rows);
        return result.rows[0].id;

    }

    async deleteSongsById(id) {
        const query = {
            text : 'DELETE FROM songs WHERE id = $1 RETURNING id',
            values : [id],
        };
        const result = await this._pool.query(query);

        if (!result.rows.length){
            throw new NotFoundError ('Lagu gagal dihapus. Id tidak ditemukan')
        }
    }
}

module.exports = SongService;