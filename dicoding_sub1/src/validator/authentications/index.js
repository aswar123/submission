const {
  PostAuthenticationPayloadSchema,
  PutAuthenticationPayloadSchema,
  DeleteAuthenticationPayloadSchema,
} = require('./schemaAuthentications');

const InvariantError = require('../../services/exceptions/InvariantError');
const { payload } = require('@hapi/hapi/lib/validation');

const AuthenticationsValidator = {
    validatePostAuthenticationPayload : (payload) => {
        const validationResult = PostAuthenticationPayloadSchema.validate(payload);
        if (validationResult.error) {
            throw new InvariantError(validationResult.error.message);
        }
    },
    validatePutAuthenticationPayload: (payload) => {
        const validationResult = PutAuthenticationPayloadSchema.validate(payload);
        if (validationResult.error) {
            throw new InvariantError (validationResult.error.message);
        }
    },
    validateDeleteAuthenticationPayload: (payload) => {
        const validationResult = DeleteAuthenticationPayloadSchema.validate(payload);
        if (validationResult.error) {
            throw new InvariantError (validationResult.error.message);
        }
    },

};

module.exports = AuthenticationsValidator;