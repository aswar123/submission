const ExportOpenMusicPayloadSchema = require('./schemaexports');
const InvariantError = require('../../services/exceptions/InvariantError');


const ExportsValidator = {
    validateExportOpenMusicPayload : (payload) => {
        const validationResult = ExportOpenMusicPayloadSchema.validate(payload);

        if (validationResult.error) {
            throw new InvariantError(validationResult.error.message);
        }
    },
};

module.exports = ExportsValidator;