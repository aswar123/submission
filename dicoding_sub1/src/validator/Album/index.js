
const {AlbumPayloadSchema, IdPlaylistAlbumsSchema}= require("./schemaAlbum");
const InvariantError =require('../../services/exceptions/InvariantError');
const { payload } = require("@hapi/hapi/lib/validation");


const AlbumValidator = {
    validateAlbumPayload : (payload) => {
        const validationResult = AlbumPayloadSchema.validate(payload);
        if(validationResult.error){
            throw new InvariantError(validationResult.error.message);
        }
    },
    validateIdPlaylistPayload : (payload) => {
        const validationResult = IdPlaylistAlbumsSchema.validate(payload);
        if(validationResult.error){
            throw new InvariantError(validationResult.error.message);
        }
    },
};

module.exports = AlbumValidator;