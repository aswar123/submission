const Joi = require('joi');

const AlbumPayloadSchema = Joi.object({
    name : Joi.string().required(),
    year : Joi.number().required(),
});

const IdPlaylistAlbumsSchema = Joi.object({
    id : Joi.string().required(),
});

module.exports = {AlbumPayloadSchema, IdPlaylistAlbumsSchema};