const { SongsPayloadSchema } = require("./schemaSongs");
const InvariantError =require('../../services/exceptions/InvariantError');
const { payload } = require("@hapi/hapi/lib/validation");



const SongsValidator = {
    validateSongsPayload : (payload) => {
        const validationResult = SongsPayloadSchema.validate(payload);
        if (validationResult.error){
            throw new InvariantError(validationResult.error.message);
        }
    },
};

module.exports = 
    SongsValidator;