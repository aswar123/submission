const { payload } = require('@hapi/hapi/lib/validation');
const InvariantError =require('../../services/exceptions/InvariantError');
const {UserPayloadSchema} = require('./schemaUsers');


const UsersValidator = {
    validateUserPayload : (payload) => {
        const validationResult = UserPayloadSchema.validate(payload);

        if (validationResult.error) {
            throw new InvariantError (validationResult.error.message);
        }
    },
};

module.exports = UsersValidator;