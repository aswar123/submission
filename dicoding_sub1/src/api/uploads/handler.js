const ClientError = require("../../services/exceptions/ClientError");


class UploadsHandler {
    constructor ( albumService, storageService, validator) {
        this._storageService = storageService;
        this._validator = validator;
        this._AlbumService = albumService;

        this.postUploadImagesHandler = this.postUploadImagesHandler.bind(this);
    }

    async postUploadImagesHandler(request, h) {
        try {
            const {cover} = request.payload;
            const {albumId} = request.params;
            await this._validator.validateImageHeaders(cover.hapi.headers);
            const fileName = await this._storageService.writeFile(cover, cover.hapi);
            console.log(albumId);;
            await this._AlbumService.insertToCover(albumId, fileName);
            const response = h.response ({
                status : 'success',
                message : 'Sampul berhasil diunggah',
                data : {
                    coverURL : `http://${process.env.HOST}:${process.env.PORT}/src/api/uploads/file/images/${fileName}`,
                },
            });
            response.code(201);
            return response;

        } catch (error) {
            if (error instanceof ClientError) {
                const response = h.response ({
                status : 'fail',
                message : error.message,
            });
            response.code(error.statusCode);
            return response;
            }

            const response = h.response ({
                status : 'error',
                message : 'Maaf, terjadi kegagalan pada server kami.',
            });
            response.code(500);
            console.log(error);
            return response;
            
        }

    }
}

module.exports = UploadsHandler;