const { handler } = require("@hapi/hapi/lib/cors");
const path = require('path');

const routes = (handler) => [
    {
        method : 'POST',
        path : '/albums/{albumId}/covers',
        handler : handler.postUploadImagesHandler,
        options : {
            payload : {
                allow : 'multipart/form-data',
                multipart : true,
                output : 'stream',
                maxBytes : 500000,
            },
        },
    },
    {
        method : 'GET',
        path : '/src/api/uploads/file/{param*}',
        handler : {
            directory : {
                path : path.resolve(__dirname, 'images'),
            },
        },
    },
];

module.exports = routes;