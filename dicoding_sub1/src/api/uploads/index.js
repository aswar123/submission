const { server } = require("@hapi/hapi");
const routes = require("./routes");
const UploadsHandler = require('./handler');

module.exports = {
    name : 'uploads',
    version : '1.0.0',
    register : async (server, {albumservice, storageService, validator,}) => {
        const uploadsHandler = new UploadsHandler(albumservice, storageService, validator );
        server.route(routes(uploadsHandler));
    },
};