const { Client } = require("pg");
const ClientError = require('../../services/exceptions/ClientError');

class UserHandler {
    constructor(service, validator) {
        this._service = service;
        this._validator = validator;
        
        this.postUserHandler = this.postUserHandler.bind(this);
    }

    async postUserHandler (request, h) {
        try{
            this._validator.validateUserPayload(request.payload);
            const {username, password, fullname} = request.payload;
            const UserId= await this._service.addUser({username, password, fullname});
            console.log(UserId);

            const response = h.response({
                status : 'success',
                message : 'User berhasil di tambahkan',
                data : {
                    userId: UserId,
                },
            });
            response.code(201);
            return response;
        } catch (error) {
            if (error instanceof ClientError) {
                const response = h.response({
                    status : 'fail',
                    message : error.message,
                });
                response.code(400);
                console.log()
                return response;
            }

            const response = h.response({
                status : 'error',
                message : 'Maaf, terjadi kesalahan pada server kami.',
            });
            response.code(500);
            return response;
        }
    }
}

module.exports = UserHandler;