
const AlbumHandler = require('./handler');
const routes = require('./routes');
const service = require('../../services/postgres/AlbumService');

module.exports = {
  name: 'notes',
  version: '1.0.0',
  register: async (server, { service, validator }) => {
    const AlbumsHandler = new AlbumHandler(service, validator);
    server.route(routes(AlbumsHandler));
  },
};
