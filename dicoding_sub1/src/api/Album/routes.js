

const routes = (handler) => [
  //albums
  {
    method: 'POST',
    path: '/albums',
    handler: handler.postAlbumHandler,
  },
  {
    method: 'GET',
    path: '/albums/{id}',
    handler: handler.getAlbumHandler,
  },
  {
    method: 'PUT',
    path: '/albums/{id}',
    handler: handler.putAlbumHandler,
  },
  {
    method: 'DELETE',
    path: '/albums/{id}',
    handler: handler.deletetAlbumHandler,
  },
  {
    method : 'POST',
    path : '/albums/{id}/likes',
    handler : handler.postLikeAlbumsHandler,
    options : {
      auth : 'openmusic_jwt',
    },
  },
  {
    method : 'GET',
    path : '/albums/{id}/likes',
    handler : handler.getLikeAlbumsHandler,
  },
  
];
 
module.exports = routes;