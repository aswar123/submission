const ClientError = require('../../services/exceptions/ClientError');

class AlbumHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;
 
    this.postAlbumHandler = this.postAlbumHandler.bind(this);
    this.getAlbumHandler = this.getAlbumHandler.bind(this);
    this.putAlbumHandler = this.putAlbumHandler.bind(this);
    this.deletetAlbumHandler = this.deletetAlbumHandler.bind(this);
    this.postLikeAlbumsHandler = this.postLikeAlbumsHandler.bind(this);
    this.getLikeAlbumsHandler = this.getLikeAlbumsHandler.bind(this);
  }
async postAlbumHandler(request, h) {
    try {
      this._validator.validateAlbumPayload(request.payload);
      const {name, year } = request.payload;
      const  albumId = await this._service.AddAlbum({name, year});
 
      const response = h.response({
        status: 'success',
        message: 'Menambahkan album.',
        data: {
          albumId,
        },
      });
      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(400);
      console.error(error);
      return response;
    }
  }
 
 
  async getAlbumHandler(request, h) {
    try {
      const {id} = request.params;
      console.log('tess');
      const album = await this._service.getAlbumById(id);
      return {
        status: 'success',
        message : 'Mendapatkan album berdasarkan id.',
        data: {
          album,
        },
      };
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(404);
      console.error(error);
      return response;
    }
  }
 
  async putAlbumHandler(request, h) {
    try {
      this._validator.validateAlbumPayload(request.payload);
      const { name, year } = request.payload;
      const { id } = request.params;
      const result = await this._service.editAlbumByid(id, { name, year });
      return {
        status: 'success',
        message: 'Mengubah album berdasarkan id album.',
        data : {
          result,
        },
      };
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(400);
      console.error(error);
      return response;
    }
  }
 
  async deletetAlbumHandler(request, h) {
    try {
      const { id } = request.params;
      await this._service.deleteAlbumById(id);
      return {
        status: 'success',
        message: 'Menghapus album berdasarkan id.',
      };
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }

  async postLikeAlbumsHandler(request, h ) {
    try {
      await this._validator.validateIdPlaylistPayload(request.params);
      const {id : credentialId} = request.auth.credentials;
      const {id} = request.params;
      await this._service.verifyAlbumIdInDatabase(id);
      const like = await this._service.cekLikeAlbums(id, credentialId);
      if (like) {
        await this._service.unlikeAlbums(id, credentialId);
        const response = h.response({
          status : 'success',
          message : 'berhasil Unlike Albums',
        });
        response.code(201);
        return response;
      } else {
        await this._service.likeAlbums(id, credentialId);
        const response = h.response({
          status : 'success',
          message : 'berhasil Like Albums',
        });
        response.code(201);
        return response;
      }
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }

  async getLikeAlbumsHandler(request, h ) {
    try {
      const {id} = request.params;
      const countLike = await this._service.countLikeAlbums(id);
      console.log(countLike);
      const response = h.response({
        status : 'success',
        data : {
          likes : Number(countLike.data),
        },
      });
      console.log(countLike.source);
      if (countLike.source === 'cache'){
        response.header('X-Data-Source', 'cache');
      };
      response.code(200);
      return response;
    }catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      console.error(error);
      return response;
    }
  }
}
 
module.exports = AlbumHandler;