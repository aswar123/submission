const ClientError = require('../../services/exceptions/ClientError');

class SongsHandler {
  constructor(service, validator) {
    this._service = service;
    this._validator = validator;
 
    this.postSongsHandler = this.postSongsHandler.bind(this);
    this.getAllSongsHandler = this.getAllSongsHandler.bind(this);
    this.getSongsByIdHandler = this.getSongsByIdHandler.bind(this);
    this.putSongsHandler = this.putSongsHandler.bind(this);
    this.deleteSongsHandler = this.deleteSongsHandler.bind(this);
  }
  //addSongs
  async postSongsHandler (request, h){
    try{
      this._validator.validateSongsPayload(request.payload);
      const {title, year, genre, performer, duration, albumId} = request.payload;
        const songId = await this._service.AddSong({title, year, genre, performer, duration, albumId});
        const response = h.response({
        status : 'success',
        message : 'Menambahkan lagu',
        data : {
          songId,
        },
      });
      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        console.log('di bawah kode error');
        console.log(error.statusCode);
        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(404);
      console.error(error);
      return response;
    }
  }
 // get songs
 async getAllSongsHandler(request, h){
   const {title, performer} = request.query;
   console.log(title, performer);
   const songs = await this._service.getSongs(title, performer);
   return {
     status : 'success',
     data :{
       songs,
     },
   };
 }

 async getSongsByIdHandler (request, h){
  try {
    const {id} = request.params;
    console.log(`ini id get ${id}`);
   const song = await this._service.getSongsById(id);
   const response = h.response({
        status : 'success',
        message : 'detail lagu',
        data : {
          song
        },
      });
      response.code(200);
      return response;
  } catch (error) {
    if ( error instanceof ClientError){
      const response = h.response({
        status : 'fail',
        message : error.message,
      });
      response.code(error.statusCode);
      console.log(`ini bu id yg error${error.statusCode}`);
      return response;
    };

    const response = h.response({
      status : 'error',
      message : 'Maaf, terjadi kegagalan pada server kami.',
    }); 
    response.code(400);
    return response;
  }
 }
  
 // update songs
 
 async putSongsHandler (request, h ){
  try {
     this._validator.validateSongsPayload(request.payload);
   const {id} = request.params;
   const {title, year, genre, performer, duration, albumId} = request.payload;
   const songId = await this._service.upadateSongsById(id, {title, year, genre, performer, duration, albumId });
   return {
     status : 'success',
     message : 'Mengubah lagu berdasarkan id lagu',
     data : {
       songId,
     },
   };
   
  } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);

        return response;
      }
 
      // Server ERROR!
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(400);
      return response;
    }
 }
 // delete songs 
 async deleteSongsHandler(request, h){
   try {
     const {id} = request.params;
     console.log(id);
     await this._service.deleteSongsById(id);

     const response = h.response({
       status : 'success',
       message : 'Menghapus lagu berdasarkan id',
     });
     response.code(200);
     return response;
   } catch (error){
     if (error instanceof ClientError){
       const response = h.response({
         status : 'fail',
         message : error.message,
       });
       response.code(error.statusCode);
       console.log(error.statusCode);
       return response;
     }
     const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(400);
      console.error(error);
      return response;
    }
 }
  
}
 
module.exports = SongsHandler;