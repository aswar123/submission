
const SongsHandler = require('./handler');
const routes = require('./routes');
const service = require('../../services/postgres/AlbumService');

module.exports = {
  name: 'songs',
  version: '1.0.0',
  register: async (server, { service, validator }) => {
    const MusicHandler = new SongsHandler(service, validator);
    server.route(routes(MusicHandler));
  },
};
