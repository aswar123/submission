const ClientError = require('../../services/exceptions/ClientError');


class PlaylistHandler {
    constructor (service, validator) {
        this._service = service;
        this._validator = validator;

        this.postPlaylistHandler = this.postPlaylistHandler.bind(this);
        this.getPlaylistHandler = this.getPlaylistHandler.bind(this);
        this.deletePlaylistHandler = this.deletePlaylistHandler.bind(this);
        this.postPlaylistIdSongHandler = this.postPlaylistIdSongHandler.bind(this);
        this.deletePlaylistIdSongHandler = this.deletePlaylistIdSongHandler.bind(this);
        this.getPlaylistiIdSongHandler = this.getPlaylistiIdSongHandler.bind(this);
        
    };
    

    async postPlaylistHandler (request, h ) {
        try{
            this._validator.validatePostPlaylistpayload(request.payload);
            const {name} = request.payload;
            const {id:credentialId} = request.auth.credentials;
            const playlistId = await this._service.addPlaylist({
                name, owner: credentialId,
            });
            const response = h.response({
                status : 'success',
                message : 'Playlist berhasil ditambahkan',
                data : {
                    playlistId,
                },
            });
            response.code(201);
            return response;
        }catch(error) {
            if (error instanceof ClientError) {
                const response = h.response ({
                    status : 'fail',
                    message : error.message,
                });
                response.code(error.statusCode);
                return response;
            }
             // Server ERROR!
            const response = h.response({
                status: 'fail',
                message: 'Maaf, terjadi kegagalan pada server kami.',
            });
            response.code(400);
            return response;
        }
    }
    async getPlaylistHandler (request, h) {
        try {
            const {id: credentialId} = request.auth.credentials;
        const Playlists = await this._service.getPlaylists(credentialId);
        return {
            status : 'success',
            data : {
                playlists : Playlists,
            },
        };
        } catch (error) {
            if (error instanceof ClientError) {
                const response = h.response({
                    status : 'fail',
                    message : error.message,
                });
                response.code(error.statusCode);
                return response;
            }
        }
    }

    async getPlaylistiIdSongHandler (request, h) {
        try{
            const {id} = request.params;
            const {id: credentialId} = request.auth.credentials;
            await this._service.validateAcceess(credentialId, id);
            const Playlists = await this._service.getSongsFromPlaylist(id);
            return {
                status : 'success',
                data : {
                    playlist : Playlists,
                },
            };
        }catch (error) {
            if (error instanceof ClientError) {
                const response = h.response({
                    status : 'fail',
                    message : error.message,
                });
                response.code(error.statusCode);
                return response;
            }
        }
    }
   
async postPlaylistIdSongHandler (request, h) {
    try {
        this._validator.validatePostSongPayload(request.payload);
        const {id} = request.params;
        const {songId} = request.payload;
        const {id : credentialId} = request.auth.credentials;
        await this._service.verifySongInDatabase(songId);
        await this._service.validateAcceess(credentialId, id);
        await this._service.addSongToPlaylist(id, songId);
        const response = h.response ({
            status : 'success',
            message : 'Lagu berhasil di tambahkan ke plalist',
        });
        response.code(201);
        return response;
    } catch (error) {
        if (error instanceof ClientError ) {
            const response = h.response ({
                status : 'fail',
                message : error.message,
            });
            response.code(error.statusCode);
            return response;
        }

        const response = h.response ({
            status : 'fail',
            message : 'Maaf, terjadi kegagalan pada server kami.',
        });
        response.code(500);
        return response;
    }
}
 async deletePlaylistHandler (request, h) {
        try {
        const {id } = request.params;
        const {id: credentialId} = request.auth.credentials;
         await this._service.validateAcceess(credentialId, id);
        await this._service.deletePlaylistById(id);
        return {
            status : 'success',
            message: 'Playlist berhasil di hapus',
        };
        } catch (error) {
            if (error instanceof ClientError) {
                const response = h.response({
                    status : 'fail',
                    message : error.message,
                });
                response.code(error.statusCode);
                return response;
            }
        const response = h.response ({
            status : 'error',
            message : 'Maaf, terjadi kegagalan pada server kami.',
        });
        response.code(500);
        return response;
    }
}

async deletePlaylistIdSongHandler (request, h) {
    try {
        const {id} = request.params;
        const {songId} = request.payload;
        const {id: credentialId } = request.auth.credentials;
        await this._service.validateAcceess(credentialId, id);
        await this._service.deleteSongFromPlaylist(id, songId);

        return {
            status : 'success',
            message : 'Lagu berhasil dihapus dari playlist',
        };
    } catch (error) {
        if (error instanceof ClientError) {
            const response = h.response({
                status : 'fail',
                message : error.message,
            });
            response.code(error.statusCode);
            return response;
        }

        const response = h.response ({
            status : 'error',
            message : 'Maaf. terjadi kegagalan pada server kami.',
        });
        response.code(500);
        console.log(error);
        return response;
    }
}

}

module.exports = PlaylistHandler; 