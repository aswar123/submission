
const routes = (handler) => [
    {
        method : 'POST',
        path : '/playlists',
        handler : handler.postPlaylistHandler,
        options : {
            auth : 'openmusic_jwt',
        },
    },
    {
        method : 'GET',
        path : '/playlists',
        handler : handler.getPlaylistHandler,
        options : {
            auth : 'openmusic_jwt',
        },

    },
    {
        method : 'DELETE',
        path : '/playlists/{id}',
        handler : handler.deletePlaylistHandler,
        options : {
            auth : 'openmusic_jwt',
        },

    },
    {
        method : 'POST',
        path : '/playlists/{id}/songs',
        handler : handler.postPlaylistIdSongHandler,
        options : {
            auth : 'openmusic_jwt',
        },

    },
    {
        method : 'DELETE',
        path : '/playlists/{id}/songs',
        handler : handler.deletePlaylistIdSongHandler,
        options : {
            auth : 'openmusic_jwt',
        },
    },
    {
        method : 'GET',
        path : '/playlists/{id}/songs',
        handler : handler.getPlaylistiIdSongHandler,
        options : {
            auth : 'openmusic_jwt'
        },
    },
];

module.exports = routes;