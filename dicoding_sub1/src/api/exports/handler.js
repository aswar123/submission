const ClientError = require("../../services/exceptions/ClientError");




class ExportsHandler {
    constructor (service, validator, playlistsService) {
        this._service = service;
        this._validator = validator;
        this._playlistsService = playlistsService;

        this.postExportOpenMusicHandler = this.postExportOpenMusicHandler.bind(this);
    }

    async postExportOpenMusicHandler (request, h ) {
        try {
            this._validator.validateExportOpenMusicPayload(request.payload);
            const {id : credentialId} = request.auth.credentials;
            const {playlistId} = request.params;
            await this._playlistsService.validateAcceess(credentialId, playlistId);
            console.log(request.payload.targetEmail);
            const message = {
                playlistId,
                targetEmail : request.payload.targetEmail,
            };
            await this._service.sendMessage('export:openmusic', JSON.stringify(message));

            const response = h.response ({
                status : "success",
                message : 'Permintaan Anda sedang kami proses',
            });
            response.code(201);
            return response;
        }catch (error) {
            if (error instanceof ClientError) {
                 const response = h.response ({
                    status : 'fail',
                    message : error.message,
                });
                response.code(error.statusCode);
                return response;
            }

            const response = h.response ({
                status : 'error',
                message : 'maaf',
            });
            response.code(500);
            console.error(error);
            return response;
        }
    }
}


module.exports = ExportsHandler;