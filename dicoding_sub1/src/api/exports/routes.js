const { handler } = require("@hapi/hapi/lib/cors");

const routes = (handler) => [
    {
        method : 'POST',
        path : '/export/playlists/{playlistId}',
        handler : handler.postExportOpenMusicHandler,
        options : {
            auth : 'openmusic_jwt',
        },
    },
];

module.exports = routes;