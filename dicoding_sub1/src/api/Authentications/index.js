const AuthenticationHandler = require('./handler');
const routes = require('./routes');

module.exports = {
    name : 'authentications',
    version : '1.0.0',
    register: async (server, {
        authenticationsSerivice,
        usersService,
        tokenManager,
        validator,
    }) => {
        const  authenticationsHandler = new AuthenticationHandler(
        authenticationsSerivice,
        usersService,
        tokenManager,
        validator,
        );
        server.route(routes(authenticationsHandler));
    },
};