require ('dotenv').config();
const Hapi = require('@hapi/hapi');
const Jwt = require('@hapi/jwt');
const path = require('path');
const Inert = require('@hapi/inert');

// Auth
const AuthenticationsService = require('./src/services/postgres/AuthenticationsService');
const AuthenticationsValidator = require('./src/validator/authentications');
const Authentications = require('./src/api/Authentications');
const TokenManager = require('./src/tokenize/TokenManager');
// Album
const Album = require('./src/api/Album');
const AlbumService = require('./src/services/postgres/AlbumService');
const AlbumValidator = require('./src/validator/Album');
// Songs
const Song = require('./src/api/Songs');
const SongService = require('./src/services/postgres/SongService');
const SongValidator = require('./src/validator/songs');
//const SongsService = new AlbumService();
//users
const users = require('./src/api/users');
const UsersService = require('./src/services/postgres/UsersService');
const UsersValidator = require('./src/validator/users');
//laylists
const playlists = require('./src/api/playlists');
const PlaylistsService = require('./src/services/postgres/PlaylistsService');
const PlaylistsValidator = require('./src/validator/playlists');
//exports
const _exports = require('./src/api/exports');
const ProducerService = require('./src/services/rabbitmq/ProducerService');
const ExportsValidator = require('./src/validator/exports');
//uploads
const uploads = require('./src/api/uploads');
const StorageService = require('./src/services/storage/StorageService');
const UploadsValidator = require('./src/validator/uploads');
//cache
const CacheService = require('./src/services/redis/CacheService');

const storageService = new StorageService(path.resolve(__dirname, 'src/api/uploads/file/images'));
const cacheService = new CacheService();
const songservice = new SongService();
const albumservice = new AlbumService(cacheService, storageService );
const authenticationsSerivice = new AuthenticationsService();
const usersService = new UsersService();
const playlistsService = new PlaylistsService();

const init = async () => {
  const server = Hapi.server({
      port : process.env.PORT,
      host : process.env.HOST,
      routes : {
      cors: {
          origin : ['*'],
      },
      },
  });

  await server.register([
    {
      plugin : Jwt,
    },
    {
      plugin : Inert,
    },
  ]);

  server.auth.strategy('openmusic_jwt', 'jwt', {
    keys : process.env.ACCESS_TOKEN_KEY,
    verify : {
      aud: false,
      iss : false,
      sub : false,
      maxAgeSec: process.env.ACCESS_TOKEN_AGE,
    },
    validate : (artifacts) => ({
      isValid: true,
      Credentials: {
        id: artifacts.decoded.payload.id,
      },
    }),
  });

  // registrasi satu plugin
  await server.register([
    {
    plugin: Album,
    options: { 
      service: albumservice,
      validator: AlbumValidator,
   },
  },
  {
    plugin : Song,
    options : {
      service : songservice,
      validator : SongValidator,
    },
  },
  {
    plugin : playlists,
    options : {
      service : playlistsService,
      validator : PlaylistsValidator,
    },
  },
  {
    plugin : users,
    options : {
      service : usersService,
      validator : UsersValidator,
    },
  },
  {
    plugin : Authentications,
    options : {
      authenticationsSerivice,
      usersService,
      tokenManager: TokenManager,
      validator : AuthenticationsValidator,

    },
  },
  {
    plugin : _exports,
    options : {
      service : ProducerService,
      validator : ExportsValidator, 
      playlistsService,
    },
  },
  {
    plugin : uploads,
    options : {
      albumservice,
      storageService,
     validator : UploadsValidator,
    },
  },
  ]);
 
  await server.start();
  console.log(`Server berjalan pada ${server.info.uri}`);
};
 
init();

